#！/bin/bash

delete_fun(){
  flag=$1
  elements=$(ps -aux | grep $flag | awk '{print $2}')
  for var in $elements
  do
    kill $var
  done
}

delete_fun orderer
delete_fun peer
rm -rf /var/hyperledger
rm -rf crypto-config
rm -rf mychannel.block
rm -rf peer
rm -rf orderer
rm -rf config