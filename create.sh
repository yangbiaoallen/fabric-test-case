#!/bin/bash

source env.sh

# generate certs
bash generate.sh

# start orderer
bash start_orderer.sh &

# start peer
bash start_peer.sh &

#wait 10 seconds
sleep 10

# create channel
bash join_channel.sh

