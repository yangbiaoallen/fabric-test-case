# fabric-test-env

> This project is for `testing` and `development` only.
> Do NOT use in production!

An all in one fabric with 1 peer, 1 orderer and 1 channel.

### Prerequisites:

#### 1.the fabric binrary is in the system env

### Test Process:

#### 1.load environment
  $  source env.sh

#### 2.create test
  $ bash create.sh

#### 3.delete test
  $ bash delete.sh